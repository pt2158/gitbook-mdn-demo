import openpyxl
import datetime

wb = openpyxl.load_workbook('D:\\Archive\\tmp2\\py\\xl\\sample01.xlsx')
ws = wb['Sheet1']

c1 = ws['A10']
c1.value = datetime.datetime(2021, 4, 30)
c1.number_format = 'yyyy/mm/dd'

c2 = ws['B10']
c2.value = '鈴木商店'

c3 = ws['C10']
c3.value = 'B'

c4 = ws['D10']
c4.value = 3800
c4.number_format = '#,##0'

c5 = ws['E10']
c5.value = 12

c6 = ws['F10']
c6.value = '=D10*E10'
c6.number_format = '#,###'

wb.save('D:\\Archive\\tmp2\\py\\xl\\sample01_add.xlsx')